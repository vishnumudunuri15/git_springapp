package com.nt.aspect;

import javax.annotation.Resource;
import javax.inject.Named;

import com.nt.dao.AuthenticationDAO;

@Named("authManager")
public class AuthenticationManager {
	@Resource
	private AuthenticationDAO dao;

	ThreadLocal<UserDetails> local = new ThreadLocal();

	public boolean isAuthenticated() {
		System.out.println("3");
		UserDetails details = local.get();
		boolean flag = false;
		flag = dao.authenticate(details);
		System.out.println("5");
		return flag;
	}

	public void signIn(String user, String pwd) {
		System.out.println("1");
		UserDetails details = new UserDetails();
		details.setUsername(user);
		details.setPassword(pwd);
		local.set(details);
	}
}
