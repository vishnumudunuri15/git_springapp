package com.nt.dao;

import javax.annotation.Resource;
import javax.inject.Named;

import org.springframework.jdbc.core.JdbcTemplate;

import com.nt.aspect.UserDetails;

@Named("authDAO")
public class AuthenticationDAO {
	private static final String AUTH_QUERY = "SELECT COUNT(*) FROM USER_DOCS WHERE USERNAME=? AND PASSWORD=?";

	@Resource
	private JdbcTemplate template;

	public boolean authenticate(UserDetails details) {
		System.out.println("4");
		int count = template.queryForObject(AUTH_QUERY, Integer.class,
				details.getUsername(), details.getPassword());
		if (count == 0)
			return false;
		else
			return true;
	}

}
