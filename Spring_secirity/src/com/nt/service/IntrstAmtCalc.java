package com.nt.service;

import javax.inject.Named;

@Named("service")
public class IntrstAmtCalc {

	public float amtCalculator(float pAmt, float time, float rate) {

		return (pAmt * time * rate) / 100;
	}

}
