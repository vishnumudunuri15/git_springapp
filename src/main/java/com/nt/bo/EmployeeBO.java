package com.nt.bo;

public class EmployeeBO {
	private int EMPNO;
	private String EMPNAME, EMPADDR;

	public int getEMPNO() {
		return EMPNO;
	}

	public void setEMPNO(int eMPNO) {
		EMPNO = eMPNO;
	}

	public String getEMPNAME() {
		return EMPNAME;
	}

	public void setEMPNAME(String eMPNAME) {
		EMPNAME = eMPNAME;
	}

	public String getEMPADDR() {
		return EMPADDR;
	}

	public void setEMPADDR(String eMPADDR) {
		EMPADDR = eMPADDR;
	}

	@Override
	public String toString() {
		return "EmployeeBO [EMPNO=" + EMPNO + ", EMPNAME=" + EMPNAME
				+ ", EMPADDR=" + EMPADDR + "]";
	}

}
