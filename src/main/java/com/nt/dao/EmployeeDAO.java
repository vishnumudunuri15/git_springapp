package com.nt.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Named;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.nt.bo.EmployeeBO;

@Named("empDAO")
public class EmployeeDAO {
	private static final String GET_EMP_DETAILS_BY_ADDR = "SELECT EMPNO,EMPNAME,EMPADDR FROM EMPLOYEE WHERE EMPADDR=:EMPADDR";
	@Resource
	private NamedParameterJdbcTemplate template;

	public List<EmployeeBO> getDetails(String EMPADDR) {
		Map<String, Object> map = new HashMap();
		map.put("EMPADDR", EMPADDR);

		List<EmployeeBO> listbo = new ArrayList();
		template.query(GET_EMP_DETAILS_BY_ADDR, map, new MyRowCallHandler(listbo));

		return listbo;

	}

	public static class MyRowCallHandler implements RowCallbackHandler {
		private List<EmployeeBO> listbo = null;

		public MyRowCallHandler(List<EmployeeBO> listbo) {

			this.listbo = listbo;
		}

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			EmployeeBO bo = new EmployeeBO();
			bo.setEMPNO(rs.getInt(1));
			bo.setEMPNAME(rs.getString(2));
			bo.setEMPADDR(rs.getString(3));
			listbo.add(bo);

		}

	}

}
