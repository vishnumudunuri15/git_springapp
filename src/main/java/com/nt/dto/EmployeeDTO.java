package com.nt.dto;

import java.io.Serializable;

public class EmployeeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int EMPNO;
	private String EMPNAME, EMPADDR;
	
	/*public EmployeeDTO(int eMPNO, String eMPNAME, String eMPADDR) {
		super();
		EMPNO = eMPNO;
		EMPNAME = eMPNAME;
		EMPADDR = eMPADDR;
	}*/

	

	

	/*public EmployeeDTO() {
		// TODO Auto-generated constructor stub
	}*/

	public int getEMPNO() {
		return EMPNO;
	}

	public void setEMPNO(int eMPNO) {
		EMPNO = eMPNO;
	}

	public String getEMPNAME() {
		return EMPNAME;
	}

	public void setEMPNAME(String eMPNAME) {
		EMPNAME = eMPNAME;
	}

	public String getEMPADDR() {
		return EMPADDR;
	}

	public void setEMPADDR(String eMPADDR) {
		EMPADDR = eMPADDR;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [EMPNO=" + EMPNO + ", EMPNAME=" + EMPNAME
				+ ", EMPADDR=" + EMPADDR + "]";
	}

}
