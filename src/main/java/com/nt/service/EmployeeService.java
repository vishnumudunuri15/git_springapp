package com.nt.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;

import org.springframework.beans.BeanUtils;

import com.nt.bo.EmployeeBO;
import com.nt.dao.EmployeeDAO;
import com.nt.dto.EmployeeDTO;

@Named("empService")
public class EmployeeService {
	@Resource
	private EmployeeDAO dao;

	public List<EmployeeDTO> fetchDetails(String EMPADDR) {
		List<EmployeeDTO> listdto = null;
		EmployeeDTO dto = null;
		List<EmployeeBO> listbo = dao.getDetails(EMPADDR);
		listdto = new ArrayList();
		for (EmployeeBO bo : listbo) {
			dto = new EmployeeDTO();
			BeanUtils.copyProperties(bo, dto);
			listdto.add(dto);

		}

		return listdto;
	}

}
