package com.nt.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.nt.service.EmployeeService;

@ComponentScan(basePackages = { "com.nt.dao", "com.nt.service" })
@SpringBootApplication
public class SpringBootNamedParameterAppApplication {

	public static void main(String[] args) {
		EmployeeService service = null;
		ApplicationContext ctx = null;

		ctx = SpringApplication.run(SpringBootNamedParameterAppApplication.class, args);
		service = ctx.getBean("empService", EmployeeService.class);
		System.out.println(service.fetchDetails("kphb"));

	}
}
